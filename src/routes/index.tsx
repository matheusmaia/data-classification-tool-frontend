import { BrowserRouter, Routes as GroupRoute, Route, Navigate, useNavigate } from 'react-router-dom'

import SignIn from '../pages/SignIn';
import SingUp from '../pages/SignUp';
import Loading from '../pages/Loading';
import PageNotFound from '../pages/PageNotFound';
import Authorization from '../pages/Authorization';
import ProjectsList from '../pages/ProjectsList';
import ProjectsDetails from '../pages/ProjectsDetails';
import DataList from '../pages/DataList';
import NaiveDetail from '../pages/NaiveDetails';
import NaiveForm from '../pages/NaiveForm';
import VariablesList from '../pages/VariablesList';
import { useEffect } from 'react';



const Routes = () => {



    return (
        <BrowserRouter>
            <GroupRoute>
                <Route path="/" element={<Navigate to='projetos' />} />
                <Route path="/projetos" element={<ProjectsList />} />
                <Route path="/projetos/:id" element={<ProjectsDetails />} />

                <Route path="/projetos/:id/variaveis" element={<VariablesList />} />

                <Route path="/projetos/:id/data" element={<DataList />} />

                <Route path="/projetos/:id/naivebayes" element={<NaiveDetail />} />
                <Route path="/projetos/:id/naivebayes/inferencia" element={<NaiveForm />} />


                {/* <Route path="/detalhar" element={<Detail />} />
                <Route path="/formulario" element={<Formulario />} /> */}
                <Route path='/login' element={<SignIn />} />
                <Route path='/cadastrar' element={<SingUp />} />
                {/* <Route path='/loading' element={<Loading />} /> */}
                {/* <Route path='/auth' element={<Authorization />} /> */}
                <Route path='*' element={<PageNotFound />} />
            </GroupRoute>
        </BrowserRouter>
    )
}

export default Routes;