import styled from 'styled-components';
import { DataGrid as DataGridMui } from '@mui/x-data-grid';
import { Typography } from '@mui/material';

export const Main = styled.div`
    width: auto;

    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: start;
    gap: 1rem;
    padding-inline: 60px;
    flex-wrap: wrap;
`;


export const Card = styled.div`
    display: flex;
    flex-direction: column;
    background: #FFFFFF;
    border-radius: 4px;
    padding: 18px;
    width: 300px;
    gap: 12px;

`;

export const CardTitle = styled(Typography).attrs({ variant: 'h6' })`

`;

export const CardDescription = styled(Typography).attrs({ variant: 'caption' })`
    color: #B0BEC5;
`;



export const Container = styled.div`
    width: auto;

    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-inline: 32px;
    padding-block: 16px;

    flex-wrap: wrap;
    gap: 8px;


    padding-top: 0px;

`;


export const TitleWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    gap: 16px;
    flex-wrap: wrap;
    align-items: center;
    button {
        height: 30px;
    }

`;

export const Title = styled.h1`
    font-weight: normal;
    text-align: start;
`;

export const Action = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    gap: 16px;
    flex-wrap: wrap;
`;