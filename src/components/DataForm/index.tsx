import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, FormControl, InputLabel, MenuItem, Select, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Action } from "./styles";
import api from "../../services/api";


const Formulario = ({ onOpenDialog }: { onOpenDialog: () => void }) => {
    const navigate = useNavigate();
    const params = useParams()

    const [openStatus, setOpenStatus] = useState(false);
    const [values, setValues] = useState<any[]>([]);
    const [formIsValid, setformIsValid] = useState(false);


    const handleCloseDialog = () => {
        setOpenStatus(false);
        clearValues();
    };

    const getVariables = async () => {
        try {
            await api.get(`/projects/` + params['id'] + `/variables/`).then(variable => {
                setValues(variable.data);
            })
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getVariables();

    }, []);

    useEffect(() => {
        checkformIsValid();
    }, [values]);

    const checkformIsValid = () => {
        let _count = 0;

        values.forEach(element => {
            if (element.value) {
                _count++;
            }
        });

        console.log(_count);
        setformIsValid(_count >= values.length - 1);
    }

    const clearValues = () => {
        setValues(
            values.map(element => {
                element.value = '';
                return element;
            }));
    };

    const handleOpenDialog = (event: any) => {
        setOpenStatus(true);
    };

    const handleFormSubmit = async () => {
        let _values: any = {};

        values.forEach(element => {
            if (element.value === "" || element.value === undefined) {
                _values[element.name] = element.value;
            }
        });

        try {
            await api.post(`/projects/` + params['id'] + `/data/`, [_values]);
            clearValues();
            setOpenStatus(false);

        } catch (error) {
            console.log(error);
        }
    }

    return (
        <FormControl variant="standard">

            <Dialog open={openStatus} onClose={handleCloseDialog} maxWidth={'xs'}>
                <DialogTitle>Adicionar Novo Registro</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Formulário para adicionar novos registros (linhas).
                    </DialogContentText>
                    <Divider style={{ paddingBlock: '8px' }} flexItem />

                    <Action>
                        {values.map((element, index) => {
                            return (

                                <FormControl variant="outlined" size="small" key={index} style={{ width: '100%' }}>
                                    <InputLabel>{element.label}</InputLabel>
                                    <Select
                                        value={element.value}
                                        onChange={(e) => {
                                            values[index].value = e.target.value;
                                            setValues([...values]);
                                        }}
                                        defaultValue={element.value}
                                        label={element.label}
                                        variant="outlined"

                                    >
                                        {element.items.map((item: any, index: number) => {
                                            return (
                                                <MenuItem key={index} value={item}>{item}</MenuItem>
                                            )
                                        }
                                        )}
                                    </Select>
                                </FormControl>
                            )
                        })}
                    </Action>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog}>Cancel</Button>
                    <Button variant="contained" disabled={!formIsValid} type="submit" onClick={handleFormSubmit}>Cadastrar</Button>
                </DialogActions>
            </Dialog>
        </FormControl>
    )
}

export default Formulario;