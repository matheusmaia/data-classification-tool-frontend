import { Container, UserGroup } from "./styles";
import LogoMarca from '../../assets/BayesClassLogo.png';
import { AccessAlarm } from "@mui/icons-material";
import { Button, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

const AppBar = () => {
    const navigate = useNavigate()

    const handleExit = () => {
        localStorage.removeItem('@User:id');
        navigate(`/login`)
    }
    return (
        <>
            <Container>
                <img src={LogoMarca} height="18px" alt="Logo Marca" />

                <UserGroup>
                    <Typography variant="body2" display="block" style={{ fontStyle: 'italic' }} color={"black"}>{localStorage.getItem('@User:Name')}</Typography>
                    <Button variant="outlined" size="small" onClick={handleExit} disableElevation > Sair </Button>
                </UserGroup>
            </Container>
        </>
    )
}

export default AppBar;
