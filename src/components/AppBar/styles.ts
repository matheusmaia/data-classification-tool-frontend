import styled from 'styled-components';

export const Container = styled.div`
    padding-block: 4px;
    padding-inline: 36px;


    height: 42px;
    width: auto;

    display: flex;
    align-items: center;
    justify-content: center;
    border: 1px;
    justify-content: space-between;
    box-shadow: 0px 1px 10px rgba(0, 0, 0, 0.12); 
`;

export const UserGroup = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
    padding: 8px;
    border-radius: 10px;
    gap: 8px;
        /* box-shadow: 0px 1px 10px rgba(0, 0, 0, 0.12);  */

`

