import styled from 'styled-components';

export const Container = styled.div`
    width: auto;

    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-inline: 32px;
    padding-block: 16px;

    flex-wrap: wrap;
    gap: 8px;


    padding-top: 0px;

`;


export const TitleWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    gap: 16px;
    flex-wrap: wrap;
    align-items: center;
    button {
        height: 30px;
    }

`;

export const Title = styled.h1`
    font-weight: normal;
    text-align: start;
`;

export const Action = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    gap: 16px;
    flex-wrap: wrap;
`;