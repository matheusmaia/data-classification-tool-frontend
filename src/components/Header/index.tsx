import { Button, Divider, Typography } from "@mui/material";
import { Container, Title, Action, TitleWrapper } from "./styles";
import { AccessAlarm } from '@mui/icons-material';

const Header = () => {
    return (
        <Container>
            <TitleWrapper>
                <Button variant="outlined" size="small" disableElevation > Voltar </Button>
                <Title> Projetos </Title>
            </TitleWrapper>

            <Action>
                {/* <Button variant="outlined" size="small" startIcon={<AccessAlarm />} disableElevation > Excluir </Button>
                <Divider orientation="vertical" flexItem /> */}
                <Button variant="text" size="small" disableElevation > Novo Projecto </Button>

            </Action>
        </Container>
    )
}

export default Header;
