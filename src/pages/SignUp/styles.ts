import styled from 'styled-components';

export const Container = styled.div`
    height: 100%;
    width: 100%;

    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    
`;

export const Content = styled.div`
    height: 100%;
    width: 355px;

    display: flex;
    align-items: center;
    justify-content: space-evenly;
    flex-direction: column;
    > * {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 8px;
    }
`;

export const Title = styled.div`
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    text-align: start;
`;