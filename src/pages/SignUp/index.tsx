import { Button, Link, Typography, TextField } from '@mui/material';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import LogoMarca from '../../assets/BayesClassLogo.png';
import api from '../../services/api';
import { Container, Content, Title } from './styles';

const Login = () => {
    const [formValue, setformValue] = useState({
        fullname: '',
        email: '',
    });

    const [loading, setLoading] = useState(false);

    const navigate = useNavigate()

    const handleChange = (event: any) => {
        setformValue({
            ...formValue,
            [event.target.name]: event.target.value
        });
    }

    const handleSubmit = async () => {

        setLoading(true);
        const data = {
            fullname: formValue.fullname,
            email: formValue.email
        }

        try {
            await api.post('/users', data);
            navigate(`/login`)
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <Container>
            <Content>
                <header>

                    <img src={LogoMarca} height="48px" alt="Logo Marca" />
                </header>
                <main>
                    <Title>
                        <Typography variant="h5" color="#37474F"> Criar uma nova Conta </Typography>
                        <Typography variant="subtitle2" color="#B0BEC5"> Criando uma conta é possivel salvar o seu trabalho e acessar em outros dispositivos </Typography>
                    </Title>

                    <TextField label="Email" size="small" name='email' onChange={handleChange} fullWidth />
                    <TextField label="Nome" size="small" name='fullname' onChange={handleChange} fullWidth />


                    <Button variant="contained" disableElevation fullWidth onClick={handleSubmit} disabled={loading}>Criar conta</Button>
                    <Button variant="outlined" disableElevation fullWidth onClick={() => { navigate(`/login`) }}> voltar </Button>
                </main>
                <footer>
                    <Typography variant="caption" color="#B0BEC5">Ferramenta de Classificação de Dados para Uso em Diferentes Áreas do Conhecimento</Typography>
                </footer>
            </Content>

        </Container>
    )
}

export default Login;
