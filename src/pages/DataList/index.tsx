import { AddOutlined } from "@mui/icons-material";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import AppBar from "../../components/AppBar";
import api from "../../services/api";
import { Main, DataGrid, Container, TitleWrapper, Title, Action } from "./styles";


const Home = () => {

    useEffect(() => {
        if (localStorage.getItem('@User:id') === null) navigate(`/login`)
    }, []);

    const [openStatus, setOpenStatus] = useState(false);
    const [values, setValues] = useState<any[]>([]);
    const [formIsValid, setformIsValid] = useState(false);
    
    const navigate = useNavigate()
    const params = useParams()

    const [columns, setColumns] = useState([])

    const [variables, setVariables] = useState<any[]>([])

    const [rows, setRows] = useState<any[]>([])

    const [open, setOpen] = useState(false);

    const [formValue, setformValue] = useState({
        name: '',
        description: '',
        label: '',
        user_id: localStorage.getItem('@User:id'),
        author: "Matheus Maia",
    });

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = async () => {
        const data = {
            name: formValue.name,
            description: formValue.description,
            label: formValue.name.replace(/\s/g, '').toLowerCase(),
            user_id: formValue.user_id,
            author: formValue.author
        }

        try {
            await api.post('/projects/', data);
            setOpen(false);

        } catch (error) {
            console.log(error);
        }
    }

    const handleChange = (event: any) => {
        setformValue({
            ...formValue,
            [event.target.name]: event.target.value
        });
    }


    //     { field: 'id', headerName: 'ID' },
    //     {
    //         field: 'name',
    //         headerName: 'Name',
    //         flex: 1,
    //     },
    //     {
    //         field: 'description',
    //         headerName: 'Descrição',
    //         flex: 2,
    //     },
    //     {
    //         field: 'dataset_total',
    //         headerName: 'Tamanho (linhas)',
    //         flex: 1,
    //     },
    //     {
    //         field: 'variables_label',
    //         headerName: 'Variavel Alvo',
    //         flex: 1,
    //     },
    //     {
    //         field: 'author',
    //         headerName: 'Criado por',
    //         flex: 1,
    //     }
    // ];

    useEffect(() => {
        api.get(`projects/` + params['id'] + '/data').then(filters => {
            let _rows: any[] = []

            filters.data.forEach((element: any, index: number) => {
                _rows.push({
                    id: index,
                    ...element
                })
            });

            setRows(_rows);
        });


        api.get(`projects/` + params['id'] + '/variables/').then(filters => {
            setVariables(filters.data);
            console.log(variables);

        })




    }, [!open, !openStatus])

    useEffect(() => {
        const _columns: any = []

        variables.map(variable => {
            _columns.push({
                field: variable['name'],
                headerName: variable['label'],
                flex: 1
            })
        })
        console.log(_columns)

        setColumns(_columns)

    }, [variables])

    const handleRowClick = (event: any) => {
        console.log(event.id);
        navigate(`/projetos/${event.id}`)
    }






    const handleCloseDialog = () => {
        setOpenStatus(false);
        clearValues();
    };

    const getVariables = async () => {
        try {
            await api.get(`/projects/` + params['id'] + `/variables/`).then(variable => {
                setValues(variable.data);
            })
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getVariables();
    }, []);

    useEffect(() => {
        checkformIsValid();
    }, [values]);

    const checkformIsValid = () => {
        let _count = 0;

        values.forEach(element => {
            if (element.value) {
                _count++;
            }
        });

        console.log(_count);
        setformIsValid(_count >= values.length - 1);
    }

    const clearValues = () => {
        setValues(
            values.map(element => {
                element.value = '';
                return element;
            }));
    };

    const handleOpenDialog = (event: any) => {
        setOpenStatus(true);
    };

    const handleFormSubmit = async () => {
        let _values: any = {};

        values.forEach(element => {
            if (element.value === "" || element.value) {
                _values[element.name] = element.value;
            }
        });

        console.log(values, "values Origin");

        console.log(_values, "values");

        try {
            await api.post(`/projects/` + params['id'] + `/data/`, [_values]);
            clearValues();
            setOpenStatus(false);

        } catch (error) {
            console.log(error);
        }
    }



    return (
        <>
            <AppBar />

            <Container>
                <TitleWrapper>
                    <Button variant="outlined" size="small" disableElevation onClick={() => { navigate(`/projetos/` + params['id']) }}> Voltar </Button>
                    <Title> Conjunto de Dados </Title>
                </TitleWrapper>
                <Action>
                    <Button variant="contained" size="medium" startIcon={<AddOutlined />} onClick={handleOpenDialog} disableElevation > Adicionar </Button>
                </Action>
            </Container>

            <DataGrid
                rows={rows}
                columns={columns}
                pageSize={7}
                rowsPerPageOptions={[10]}
                disableSelectionOnClick
            />

            <Dialog open={open} onClose={handleClose} maxWidth={'xs'} >
                <DialogTitle>Criar Novo Projeto</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Formulario para cadastrar novo projeto.
                    </DialogContentText>


                    <TextField
                        style={{ marginTop: '20px' }}
                        label="Nome"
                        variant='outlined'
                        size="small"
                        fullWidth
                        name="name"
                        onChange={handleChange}

                    />

                    <TextField
                        style={{ marginTop: '20px' }}
                        label="Descrição"
                        variant='outlined'
                        fullWidth
                        size="small"
                        name="description"
                        onChange={handleChange}

                    />

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleSubmit}>Criar Projeto</Button>
                </DialogActions>
            </Dialog>



            <FormControl variant="standard">

                <Dialog open={openStatus} onClose={handleCloseDialog} maxWidth={'xs'}>
                    <DialogTitle>Adicionar Novo Registro</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Formulário para adicionar novos registros (linhas).
                        </DialogContentText>
                        <Divider style={{ paddingBlock: '8px' }} flexItem />

                        <Action>
                            {values.map((element, index) => {
                                return (

                                    <FormControl variant="outlined" size="small" key={index} style={{ width: '100%' }}>
                                        <InputLabel>{element.label}</InputLabel>
                                        <Select
                                            value={element.value}
                                            onChange={(e) => {
                                                values[index].value = e.target.value;
                                                setValues([...values]);
                                            }}
                                            defaultValue={element.value}
                                            label={element.label}
                                            variant="outlined"

                                        >
                                            {element.items.map((item: any, index: number) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            }
                                            )}
                                        </Select>
                                    </FormControl>
                                )
                            })}
                        </Action>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseDialog}>Cancel</Button>
                        <Button variant="contained" disabled={!formIsValid} type="submit" onClick={handleFormSubmit}>Cadastrar</Button>
                    </DialogActions>
                </Dialog>
            </FormControl>
        </>
    )
}

export default Home;