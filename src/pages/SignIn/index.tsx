import { Button, Link, Typography, TextField } from '@mui/material';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import LogoMarca from '../../assets/BayesClassLogo.png';
import api from '../../services/api';
import { Container, Content, Title } from './styles';


const SignIn = () => {

    const navigate = useNavigate()


    const [formValue, setformValue] = useState({
        email: '',
    });

    const handleSubmit = async () => {
        const data = {
            email: formValue.email
        }

        console.log(data);
        try {
            await api.post(`/users/login`, data).then(variable => {
                localStorage.setItem('@User:id', variable.data.id)
                localStorage.setItem('@User:Name', variable.data.fullname)

                navigate(`/`)
            })

        } catch (error) {
            console.log(error);
        }
    }

    const handleChange = (event: any) => {
        console.log(event);
        setformValue({
            ...formValue,
            [event.target.name]: event.target.value
        });
    }


    return (
        <Container>
            <Content>
                <header>
                    <img src={LogoMarca} height="48px" alt="Logo Marca" />
                </header>
                <main>
                    <Title>
                        <Typography variant="h5" color="#37474F"> Bem-Vindo de Volta !! </Typography>
                        <Typography variant="subtitle2" color="#B0BEC5">Por-favor entre com seu email cadastrado no sistema.</Typography>
                    </Title>

                    <TextField label="Email" name='email' onChange={handleChange} size="small" fullWidth />
                    <Button variant="contained" onClick={handleSubmit} disableElevation fullWidth>Entrar</Button>
                    <Typography variant="caption" color="#B0BEC5">ou</Typography>
                    <Button variant="outlined" disableElevation fullWidth onClick={() => { navigate(`/cadastrar`) }}> Cadastrar </Button>
                </main>
                <footer>
                    <Typography variant="caption" color="#B0BEC5">Ferramenta de Classificação de Dados para Uso em Diferentes Áreas do Conhecimento</Typography>
                </footer>
            </Content>

        </Container>
    )
}

export default SignIn;
