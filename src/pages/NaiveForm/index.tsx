import { AccessAlarm } from "@mui/icons-material";
import { Button, TextField, Typography } from "@mui/material";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AppBar from "../../components/AppBar";
import Header from "../../components/Header";
import { Main, Card, CardTitle, CardDescription } from "./styles";


const Formulario = () => {

    const navigate = useNavigate()

    useEffect(() => {
        if (localStorage.getItem('@User:id') === null) navigate(`/login`)
    }, []);
    return (
        <>
            <AppBar />
            <Header />
            <Main>
                <Card>
                    <CardTitle>Titulo</CardTitle>
                    <CardDescription> (Descrição) Foi enviado o codigo de autorização para o email cadastrado. </CardDescription>
                    <TextField
                        label={"Variavel"}
                        variant='outlined'
                        size='small'
                        fullWidth
                        defaultValue={"primeira opção"}
                    />
                </Card>

                <Card>
                    <CardTitle>Titulo</CardTitle>
                    <CardDescription> (Descrição) Foi enviado o codigo de autorização para o email cadastrado. </CardDescription>
                    <TextField
                        label={"Variavel"}
                        variant='outlined'
                        size='small'
                        fullWidth
                        defaultValue={"primeira opção"}
                    />
                </Card>

                <Card>
                    <CardTitle>Titulo</CardTitle>
                    <CardDescription> (Descrição) Foi enviado o codigo de autorização para o email cadastrado. </CardDescription>
                    <TextField
                        label={"Variavel"}
                        variant='outlined'
                        size='small'
                        fullWidth
                        defaultValue={"primeira opção"}
                    />
                </Card>

                <Card>
                    <CardTitle>Titulo</CardTitle>
                    <CardDescription> (Descrição) Foi enviado o codigo de autorização para o email cadastrado. </CardDescription>
                    <TextField
                        label={"Variavel"}
                        variant='outlined'
                        size='small'
                        fullWidth
                        defaultValue={"primeira opção"}
                    />
                </Card>

                <Card>
                    <CardTitle>Titulo</CardTitle>
                    <CardDescription> (Descrição) Foi enviado o codigo de autorização para o email cadastrado. </CardDescription>
                    <TextField
                        label={"Variavel"}
                        variant='outlined'
                        size='small'
                        fullWidth
                        defaultValue={"primeira opção"}
                    />
                </Card>

                <Card>
                    <CardTitle>Titulo</CardTitle>
                    <CardDescription> (Descrição) Foi enviado o codigo de autorização para o email cadastrado. </CardDescription>
                    <TextField
                        label={"Variavel"}
                        variant='outlined'
                        size='small'
                        fullWidth
                        defaultValue={"primeira opção"}
                    />
                </Card>

                <Card>
                    <CardTitle>Titulo</CardTitle>
                    <CardDescription> (Descrição) Foi enviado o codigo de autorização para o email cadastrado. </CardDescription>
                    <TextField
                        label={"Variavel"}
                        variant='outlined'
                        size='small'
                        fullWidth
                        defaultValue={"primeira opção"}
                    />
                </Card>
            </Main>
        </>
    )
}


export default Formulario;