import { AccessAlarm } from "@mui/icons-material";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, dialogContentTextClasses, DialogTitle, Divider, FormControl, InputLabel, Link, MenuItem, Paper, Select, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import AppBar from "../../components/AppBar";
import Header from "../../components/Header";
import api from "../../services/api";
import { Main, DataGrid, TitleCard, Card, LabelRow, RowValue, RowTitle, CardAction, Container, TitleWrapper, Title, Action, WrapperDialog, ResultAfericao } from "./styles";


const Home = () => {
    const navigate = useNavigate()

    useEffect(() => {
        if (localStorage.getItem('@User:id') === null) navigate(`/login`)
    }, []);


    const params = useParams()


    const [detail, setDetail] = useState({
        author: '',
        dataset_total: '',
        description: '',
        name: '',
        id: '',
        variables_values: [],
        variables_label: '',
        dataset_columns: [],
        naivebayes_model: ''
    });


    const getDetail = async () => {
        api.get(`/projects/` + params['id']).then(filters => {
            setDetail({
                ...detail,
                ...filters.data
            });
        })
    }
    useEffect(() => {

        getDetail()
        api.get(`/projects/` + params['id'] + `/run/bayes`).then(filters => {
        })
    }, [])


    const CardRow = ({ title, children }: any) => (
        <LabelRow>
            <RowTitle> {title}: </RowTitle>
            <RowValue> {children} </RowValue >
        </LabelRow>
    )

    const [openStatus, setOpenStatus] = useState(false);
    const [values, setValues] = useState<any[]>([]);
    const [formIsValid, setformIsValid] = useState(false);
    const [valuePredict, setValuePredict] = useState();



    const handleCloseDialog = () => {
        setOpenStatus(false);
        clearValues();
    };

    const getVariables = async () => {
        try {
            await api.get(`/projects/` + params['id'] + `/variables/`).then(variable => {
                setValues(variable.data.filter((item: any) => !item.is_main))
            })
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getVariables();
    }, []);

    useEffect(() => {
        if (localStorage.getItem('@User:id') === null) navigate(`/login`)
    }, []);

    useEffect(() => {
        checkformIsValid();
    }, [values]);

    const checkformIsValid = () => {
        let _count = 0;
        values.forEach(element => {
            if (element.value) _count++;
        });
        setformIsValid(_count >= values.length);
    }

    const clearValues = () => {
        setValues(
            values.map(element => {
                element.value = '';
                return element;
            }));
    };

    const handleOpenDialog = (event: any) => {
        setOpenStatus(true);
    };

    const handleFormSubmit = async () => {
        let _values: any = {};

        values.forEach(element => {
            if (element.value === "" || element.value) _values[element.name] = element.value;
        });

        try {
            await api.post(`/projects/` + params['id'] + `/run/predict`, _values).then(variable => {
                setValuePredict(variable.data.message);
            });

        } catch (error) {
            console.log(error);
        }
    }

    return (
        <>
            <AppBar />

            <Container>
                <TitleWrapper>
                    <Button variant="outlined" size="small" disableElevation onClick={() => { navigate(`/projetos/` + params['id']) }}> Voltar </Button>
                    <Title> Classificador Naive Bayes </Title>
                </TitleWrapper>
            </Container>

            <Main>
                <Card>
                    <TitleCard> Descrição </TitleCard>
                    <RowTitle> É um algoritmo que se baseia nas descobertas de Thomas Bayes para realizar predições em aprendizagem de máquina. O termo “naive” (ingênuo) diz respeito à forma como o algoritmo analisa as características de uma base de dados: ele assume que as variaveis são independentes entre si.  </RowTitle>
                    <Link target="_blank" href="https://blog.somostera.com/data-science/naive-bayes" > Saiba mais </Link>

                </Card>

                <Card>
                    <TitleCard> Modelo </TitleCard>
                    <CardRow title="Amostra (linhas)"> {detail?.dataset_total} </CardRow>
                    <CardRow title="Variaveis (colunas)"> {detail?.dataset_columns?.length} </CardRow>
                    <CardRow title="Variavel Alvo"> {detail?.variables_label} </CardRow>

                    <LabelRow>
                        <RowTitle> Status: </RowTitle>
                        {detail?.naivebayes_model &&
                            <RowValue style={{ color: 'green' }}> {'Pronto Para Uso'} </RowValue >
                        }
                        {!detail?.naivebayes_model &&
                            <RowValue style={{ color: 'red' }}> {'Error'} </RowValue >
                        }
                    </LabelRow>
                    {/* <CardRow title="Status"> {detail.naivebayes_model ? 'Pronto Para Uso' : 'Error'} </CardRow> */}

                </Card>

                <Card>
                    <TitleCard> Aferição de Dados </TitleCard>
                    <CardRow title="Ações"></CardRow>
                    <CardAction>
                        <Button variant="outlined" size="small" startIcon={<AccessAlarm />} disableElevation onClick={handleOpenDialog} > aferir valor </Button>
                    </CardAction>
                </Card>

            </Main>


            <FormControl variant="standard">

                <Dialog open={openStatus} onClose={handleCloseDialog} maxWidth='xl' scroll={"paper"}>
                    <DialogTitle>Aferição de Valores</DialogTitle>
                    <DialogContent dividers={true} style={{ width: '80vw' }}>
                        <WrapperDialog >
                            <Action >
                                {values.map((element, index) => {
                                    return (

                                        <FormControl variant="outlined" size="small" key={index} style={{ width: '100%' }}>
                                            <InputLabel>{element.label}</InputLabel>
                                            <Select
                                                value={element.value}
                                                onChange={(e) => {
                                                    values[index].value = e.target.value;
                                                    setValues([...values]);
                                                }}
                                                defaultValue={element.value}
                                                label={element.label}
                                                variant="outlined"

                                            >
                                                {element.items.map((item: any, index: number) => {
                                                    return (
                                                        <MenuItem key={index} value={item}>{item}</MenuItem>
                                                    )
                                                }
                                                )}
                                            </Select>
                                        </FormControl>
                                    )
                                })}
                            </Action>

                            <ResultAfericao>
                                <Typography variant="body1">{detail?.variables_label}</Typography>
                                <Typography variant="h3">{valuePredict}</Typography>

                            </ResultAfericao>
                        </WrapperDialog>
                    </DialogContent>

                    <DialogActions>
                        <Button onClick={handleCloseDialog}>Fechar</Button>
                        <Button onClick={clearValues}>Limpar</Button>
                        <Button variant="contained" disabled={!formIsValid} type="submit" onClick={handleFormSubmit}>Interir</Button>
                    </DialogActions>
                </Dialog>
            </FormControl>


        </>
    )
}


export default Home;