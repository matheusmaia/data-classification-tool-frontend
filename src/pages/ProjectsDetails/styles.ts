import styled from 'styled-components';
import { DataGrid as DataGridMui } from '@mui/x-data-grid';
import { Typography } from '@mui/material';

export const Main = styled.div`
    width: auto;

    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-between;
    gap: 3rem;
    padding-inline: 64px;
`;

export const DataGrid = styled(DataGridMui)`
    display: flex;
    height: 500px !important;
    width: 100% !important;
    border: none !important;
    flex: none;

    padding-inline: 36px;
`;


export const TitleCard = styled(Typography).attrs({ variant: 'h5' })`
`;

export const RowTitle = styled(Typography).attrs({ variant: 'body1' })`
    color: #607D8B;
`;

export const RowValue = styled(Typography).attrs({ variant: 'body1' })`
    color: #B0BEC5  ;
`;

export const LabelRow = styled.div`
    display: flex;
    flex-direction: row;
    gap: 4px;
`;

export const Card = styled.div`
    display: flex;
    flex-direction: column;
    gap: 4px;
`;

export const CardAction = styled.div`
    display: flex;
    flex-direction: row;
    gap: 4px;
`;



export const Container = styled.div`
    width: auto;

    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-inline: 32px;
    padding-block: 16px;

    flex-wrap: wrap;
    gap: 8px;


    padding-top: 0px;

`;


export const TitleWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    gap: 16px;
    flex-wrap: wrap;
    align-items: center;
    button {
        height: 30px;
    }

`;

export const Title = styled.h1`
    font-weight: normal;
    text-align: start;
`;

export const Action = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    gap: 16px;
    flex-wrap: wrap;
`;
