import { AccessAlarm } from "@mui/icons-material";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, FormControl, InputLabel, MenuItem, Select, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import AppBar from "../../components/AppBar";
import api from "../../services/api";
import { Main, DataGrid, TitleCard, Card, LabelRow, RowValue, RowTitle, CardAction, Container, TitleWrapper, Title, Action } from "./styles";


const Home = () => {

    const navigate = useNavigate()

    useEffect(() => {
        if (localStorage.getItem('@User:id') === null) navigate(`/login`)
    }, []);

    const params = useParams()
    const [detail, setDetail] = useState({
        author: '',
        dataset_total: '',
        description: '',
        name: '',
        id: '',
        variables_values: [],
        variables_label: '',
        dataset_columns: [],
    });

    const [project, setProject] = useState({
        name: '',
        description: '',
        label: '',
        user_id: localStorage.getItem('@User:id'),
        author: "Matheus Maia",
    });

    const [openVariableMain, setOpenVariableMain] = useState(false);


    const [open, setOpen] = useState(false);
    const [openDelete, setOpenDelete] = useState(false);
    const [variableMain, setVariableMain] = useState<Number>();
    const [variables, setVariables] = useState<any[]>([])


    useEffect(() => {
        console.log(detail)
        api.get(`/projects/` + params['id']).then(filters => {
            setDetail({
                ...detail,
                ...filters.data
            });
        })

        api.get(`projects/` + params['id'] + '/variables/').then(filters => {
            setVariables(filters.data);
        })
    }, [!open, !openVariableMain])

    const handleChange = (event: any) => {
        setProject({
            ...project,
            [event.target.name]: event.target.value
        });
    }
    const handleClose = () => {
        setOpen(false);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleSubmitEdit = async () => {
        const data = {
            name: project.name,
            description: project.description,
            label: project.name.replace(/\s/g, '').toLowerCase(),
            user_id: project.user_id,
            author: project.author,

        }

        try {
            await api.post(`/projects/` + params['id'], data);
            setOpen(false);

        } catch (error) {
            console.log(error);
        }
    }


    const handleVariableMainClose = () => {
        setOpenVariableMain(false);
    };

    const handleClickVariableMainOpen = () => {
        setOpenVariableMain(true);
    };

    const handleVariableMainChange = (event: any) => {
        console.log(event.target.value);
        setVariableMain(event.target.value);
    }

    const handleVariableMainSubmit = async () => {
        try {
            await api.post(`/projects/` + params['id'] + `/setVariableMain/`, { id: variableMain });
            setOpenVariableMain(false);

        } catch (error) {
            console.log(error);
        }
    }

    const handleDeleteClose = () => {
        setOpenDelete(false);
    };

    const handleClickDeleteOpen = () => {
        setOpenDelete(true);
    };

    const handleDeleteSubmit = async () => {
        try {
            await api.delete(`/projects/` + params['id']);
            navigate('/projetos')
        } catch (error) {
            console.log(error);
        }
    }

    const CardRow = ({ title, children }: any) => (
        <LabelRow>
            <RowTitle> {title}: </RowTitle>
            <RowValue> {children} </RowValue >
        </LabelRow>
    )



    const [openStatus, setOpenStatus] = useState(false);
    const [values, setValues] = useState<any[]>([]);
    const [formIsValid, setformIsValid] = useState(false);


    const handleCloseDialog = () => {
        setOpenStatus(false);
        clearValues();
    };

    const getVariables = async () => {
        try {
            await api.get(`/projects/` + params['id'] + `/variables/`).then(variable => {
                setValues(variable.data);
            })
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getVariables();

    }, []);

    useEffect(() => {
        checkformIsValid();
    }, [values]);

    const checkformIsValid = () => {
        let _count = 0;

        values.forEach(element => {
            if (element.value) {
                _count++;
            }
        });

        console.log(_count);
        setformIsValid(_count >= values.length - 1);
    }

    const clearValues = () => {
        setValues(
            values.map(element => {
                element.value = '';
                return element;
            }));
    };

    const handleOpenDialog = (event: any) => {
        setOpenStatus(true);
    };

    const handleFormSubmit = async () => {
        let _values: any = {};

        values.forEach(element => {
            if (element.value === "" || element.value) {
                _values[element.name] = element.value;
            }
        });

        try {
            await api.post(`/projects/` + params['id'] + `/data/`, [_values]);
            clearValues();
            setOpenStatus(false);

        } catch (error) {
            console.log(error);
            setOpenStatus(false);

        }
    }



    return (
        <>
            <AppBar />

            <Container>
                <TitleWrapper>
                    <Button variant="outlined" size="small" disableElevation onClick={() => { navigate('/projetos') }}> Voltar </Button>
                    <Title> Projeto: {detail.name} </Title>
                </TitleWrapper>
            </Container>

            <Main>
                <Card>
                    <TitleCard> Detalhes </TitleCard>
                    <CardRow title="Descrição"> {detail?.description} </CardRow>
                    <CardRow title="Criado por"> {detail?.author} </CardRow>
                    {/* <CardRow title="Ultima modificação"> {detail.} </CardRow> */}
                    <CardRow title="Ações"></CardRow>
                    <CardAction>
                        <Button variant="outlined" size="small" startIcon={<AccessAlarm />} disableElevation onClick={handleClickDeleteOpen}> Excluir </Button>
                        <Button variant="outlined" size="small" startIcon={<AccessAlarm />} disableElevation onClick={handleClickOpen}> Editar </Button>
                    </CardAction>
                </Card>

                <Card>
                    <TitleCard> Dados </TitleCard>
                    <CardRow title="Registros (Linhas)"> {detail?.dataset_total} </CardRow>
                    <CardRow title="Variaveis (Colunas)"> {variables?.length} </CardRow>
                    <CardRow title="Variavel Alvo"> {detail?.variables_label ? detail.variables_label : 'NECESSÁRIO ESCOLHER UMA VARIAVEL'} </CardRow>

                    <CardRow title="Ações"></CardRow>
                    <CardAction>
                        <Button variant="outlined" size="small" startIcon={<AccessAlarm />} onClick={() => { navigate('data') }} disableElevation > dados </Button>
                        <Button variant="outlined" size="small" startIcon={<AccessAlarm />} onClick={handleOpenDialog} disableElevation > inserir </Button>
                        <Button variant="outlined" size="small" startIcon={<AccessAlarm />} onClick={() => { navigate('variaveis') }} disableElevation > variaveis </Button>
                        <Button variant="outlined" size="small" startIcon={<AccessAlarm />} disableElevation onClick={handleClickVariableMainOpen} > Variavel Alvo </Button>
                    </CardAction>
                </Card>

                <Card>
                    <TitleCard> Algoritmos </TitleCard>
                    <CardRow title="Ações"></CardRow>
                    <CardAction>
                        <Button variant="outlined" size="small" startIcon={<AccessAlarm />} onClick={() => { navigate('naivebayes') }} disableElevation > NAIVE BAYES </Button>
                    </CardAction>
                </Card>

            </Main>


            <Dialog open={open} onClose={handleClose} maxWidth={'xs'} >
                <DialogTitle>Editar Projeto</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Formulario para editar novo projeto.
                    </DialogContentText>


                    <TextField
                        style={{ marginTop: '20px' }}
                        label="Nome"
                        variant='outlined'
                        size="small"
                        fullWidth
                        name="name"
                        onChange={handleChange}
                        defaultValue={detail.name}
                    />

                    <TextField
                        style={{ marginTop: '20px' }}
                        label="Descrição"
                        variant='outlined'
                        fullWidth
                        size="small"
                        name="description"
                        onChange={handleChange}
                        defaultValue={detail.description}

                    />

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleSubmitEdit}>Editar Projeto</Button>
                </DialogActions>
            </Dialog>


            <Dialog open={openDelete} onClose={handleDeleteClose} maxWidth={'xs'} >
                <DialogTitle>Deseja excluir este projeto?</DialogTitle>

                <DialogActions>
                    <Button onClick={handleDeleteClose}>Cancelar</Button>
                    <Button onClick={handleDeleteSubmit}>Sim, excluir</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openVariableMain} onClose={handleVariableMainClose}  >
                <DialogTitle>Mudar Variável Alvo:</DialogTitle>
                <DialogContent>

                    <TextField
                        style={{ marginTop: '20px' }}
                        label="Nome"
                        variant='outlined'
                        size="small"
                        fullWidth
                        name="variableMain"
                        onChange={handleVariableMainChange}
                        defaultValue={variableMain}
                        select
                    >
                        {variables.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleVariableMainClose}>Cancelar</Button>
                    <Button onClick={handleVariableMainSubmit}>Confirmar</Button>
                </DialogActions>
            </Dialog>



            <FormControl variant="standard">

                <Dialog open={openStatus} onClose={handleCloseDialog} maxWidth={'xs'}>
                    <DialogTitle>Adicionar Novo Registro</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Formulário para adicionar novos registros (linhas).
                        </DialogContentText>
                        <Divider style={{ paddingBlock: '8px' }} flexItem />

                        <Action>
                            {values.map((element, index) => {
                                return (

                                    <FormControl variant="outlined" size="small" key={index} style={{ width: '100%' }}>
                                        <InputLabel>{element.label}</InputLabel>
                                        <Select
                                            value={element.value}
                                            onChange={(e) => {
                                                values[index].value = e.target.value;
                                                setValues([...values]);
                                            }}
                                            defaultValue={element.value}
                                            label={element.label}
                                            variant="outlined"

                                        >
                                            {element.items.map((item: any, index: number) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            }
                                            )}
                                        </Select>
                                    </FormControl>
                                )
                            })}
                        </Action>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseDialog}>Cancel</Button>
                        <Button variant="contained" disabled={!formIsValid} type="submit" onClick={handleFormSubmit}>Cadastrar</Button>
                    </DialogActions>
                </Dialog>
            </FormControl>
        </>
    )
}


export default Home;