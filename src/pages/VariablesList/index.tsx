import { AddOutlined } from "@mui/icons-material";
import { Button, Chip, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, ListItem, Paper, TextField, Typography } from "@mui/material";
import axios from "axios";
import { SetStateAction, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import AppBar from "../../components/AppBar";
import api from "../../services/api";
import { Main, DataGrid, Container, TitleWrapper, Title, Action } from "./styles";

const VariablesList = () => {

    const navigate = useNavigate()
    const params = useParams()

    const [open, setOpen] = useState(false);
    const [openDelete, setOpenDelete] = useState(false);
    const [openEdit, setOpenEdit] = useState(false);
    const [labelIndex, setLabelIndex] = useState();


    const [rows, setRows] = useState([])
    const [chipData, setChipData] = useState(['Opção 1', 'Opção 2', 'Opção 3']);
    const [inputClass, setInputClass] = useState('');
    const [formValue, setformValue] = useState({
        name: '',
        description: '',
        label: '',
        user_id: localStorage.getItem('@User:id'),
        author: "Matheus Maia",
        items: [],
    });

    const [idIndex, setIdindex] = useState(0);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = async () => {
        const data = {
            label: formValue.name,
            items: chipData,
            name: formValue.name.replace(/\s/g, '').toLowerCase(),
            project_id: params['id'],
        }


        try {
            await api.post('/variables/', data);
            await setOpen(false);
        } catch (error) {
            console.log(error);
        }
    }



    const handleEditOpen = async (event: any) => {

        try {
            await api.get(`/variables/` + event.id).then(variable => {
                setLabelIndex(variable.data.label);
                setChipData(variable.data.items);
                setIdindex(variable.data.id);
                setOpenEdit(true);
                console.log(labelIndex);
                console.log(idIndex, 'efects');

                setformValue({
                    ...formValue,
                    label: variable.data.label
                });

            })
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {

    }, [!openEdit, idIndex])

    const handleEditClose = () => {
        setOpenEdit(false);
    };
    const handleEditSubmit = async () => {


        console.log(labelIndex)
        const data = {
            label: formValue.name,
            items: chipData,
            name: formValue.name.replace(/\s/g, '').toLowerCase(),
            project_id: params['id'],
        }

        console.log(data);

        try {
            await api.post(`/variables/` + idIndex, data);
            await setOpenEdit(false);

        } catch (error) {
            console.log(error);
        }
    }

    const handleChange = (event: any) => {

        setformValue({
            ...formValue,
            [event.target.name]: event.target.value
        });
    }

    // DELETAR


    const handleDeleteClose = () => {
        setOpenDelete(false);
    };

    const handleClickDeleteOpen = (event: any) => {
        setOpenDelete(true);
        setIdindex(event.id);

    };

    const handleDeleteSubmit = async () => {

        try {
            await api.delete(`/variables/` + idIndex);
            await setOpenDelete(false);
        } catch (error) {
            console.log(error);
        }
    }


    const columns = [
        {
            field: 'is_main',
            headerName: 'Principal',
            type: 'boolean',

            renderCell: (params: { value: any; }) => {
                return params.value ? <AddOutlined /> : ''
            }
        },
        {
            field: 'label',
            headerName: 'Name',
            flex: 1,
        },
        {
            field: 'items',
            headerName: 'Classes',
            flex: 3,
            type: 'singleSelect',
            renderCell: (params: { value: any[] }) => {
                return (
                    <Action>
                        {params.value.map((item: any, index: number) => (
                            <Chip key={index} label={item} />
                        ))}
                    </Action>
                )
            }

        },
        {
            field: 'actions',
            headerName: 'Ações',
            flex: 1,
            type: 'number',
            renderCell: (params: { value: any; }) => {
                return <Action>
                    <Button
                        variant="contained"
                        size="small"
                        onClick={() => { handleEditOpen(params) }}
                        disableElevation > Editar  </Button>
                    <Button
                        variant="contained"
                        size="small"
                        onClick={() => { handleClickDeleteOpen(params) }}
                        color="error"
                        disableElevation > Excluir </Button>
                </Action >
            }
        }
    ];

    const handleClassAdd = () => {
        let canAdd = true;
        chipData.forEach((item: any) => {
            console.log(item === inputClass)
            if (item === inputClass) canAdd = false;
        })

        canAdd && setChipData([...chipData, inputClass])
    }

    const handleClassAddInput = (event: any) => {
        setInputClass(event.target.value);
    }

    const handleClassDelete = (event: any) => {
        setChipData(chipData.filter((item: any, index: number) => index !== event))
    }



    useEffect(() => {
        api.get(`/projects/` + params['id'] + `/variables/`).then(filters => {
            setRows(filters.data);
        })
    }, [!openEdit, !openDelete, !open])





    return (
        <>
            <AppBar />

            <Container>
                <TitleWrapper>
                    <Button variant="outlined" size="small" disableElevation onClick={() => { navigate(`/projetos/` + params['id']) }}> Voltar </Button>

                    <Title> Variáveis (Colunas) </Title>
                </TitleWrapper>

                <Action>
                    <Button
                        variant="contained"
                        size="medium"
                        startIcon={<AddOutlined />}
                        onClick={handleClickOpen}
                        disableElevation > Nova Variável </Button>
                </Action>
            </Container>

            <DataGrid
                rows={rows}
                columns={columns}
                pageSize={7}
                rowsPerPageOptions={[10]}
                disableSelectionOnClick
            />

                <Dialog open={open} onClose={handleClose} maxWidth={'xs'} >
                    <DialogTitle>Criar Nova Variável</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Formulario para cadastrar nova variável.
                        </DialogContentText>


                        <TextField
                            style={{ marginBlock: '8px' }}
                            label="Nome"
                            variant='outlined'
                            size="small"
                            fullWidth
                            name="name"
                            onChange={handleChange}
                            defaultValue={''}

                        />

                        <Divider />
                        <Action style={{ marginBlock: '8px' }}>
                            <TextField
                                label="Adicionar Classes"
                                variant='outlined'
                                size="small"
                                onChange={handleClassAddInput}
                            />
                            <Button
                                variant="outlined"
                                size="small"
                                startIcon={<AddOutlined />}
                                onClick={handleClassAdd}
                                disableElevation >Adicionar </Button>
                        </Action>

                        <Action style={{ marginBlock: '8px' }}>
                            {chipData.map((data, index) => {
                                return (
                                    <Chip key={index}
                                        label={data}
                                        onDelete={() => { handleClassDelete(index) }}
                                    />
                                );
                            })}
                        </Action>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <Button variant="contained"  onClick={handleSubmit}>Criar Variável</Button>

                    </DialogActions>
                </Dialog>


            <Dialog open={openDelete} onClose={handleDeleteClose} maxWidth={'xs'} >
                <DialogTitle>Deseja excluir este projeto?</DialogTitle>

                <DialogActions>
                    <Button onClick={handleDeleteClose}>Cancelar</Button>
                    <Button onClick={handleDeleteSubmit}>Sim, excluir</Button>
                </DialogActions>
            </Dialog>


            <Dialog open={openEdit} onClose={handleEditClose} maxWidth={'xs'} key={idIndex} >
                <DialogTitle>Criar Nova Variável</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Formulário para cadastrar nova variável.
                    </DialogContentText>


                    <TextField
                        style={{ marginBlock: '8px' }}
                        label="Nome"
                        variant='outlined'
                        size="small"
                        fullWidth
                        name="name"
                        onChange={handleChange}
                        defaultValue={labelIndex}
                    />

                    <Divider />
                    <Action style={{ marginBlock: '8px' }}>
                        <TextField
                            label="Adicionar Classes"
                            variant='outlined'
                            size="small"
                            onChange={handleClassAddInput}

                        />
                        <Button
                            variant="outlined"
                            size="small"
                            startIcon={<AddOutlined />}
                            onClick={handleClassAdd}
                            disableElevation >Adicionar </Button>
                    </Action>

                    <Action style={{ marginBlock: '8px' }}>
                        {chipData.map((data, index) => {
                            return (
                                <Chip key={index}
                                    label={data}
                                    onDelete={() => { handleClassDelete(index) }}
                                />
                            );
                        })}
                    </Action>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleEditClose}>Cancel</Button>
                    <Button variant="contained" onClick={handleEditSubmit}>Editar Variável</Button>

                </DialogActions>
            </Dialog>
        </>
    )
}

export default VariablesList;