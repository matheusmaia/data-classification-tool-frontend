import styled from 'styled-components';
import { DataGrid as DataGridMui } from '@mui/x-data-grid';

export const Main = styled.div`
    width: auto;

    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-inline: 32px;
`;

export const DataGrid = styled(DataGridMui)`
    display: flex;
    height: 500px !important;
    width: 100% !important;
    border: none !important;
    flex: none;

    padding-inline: 36px;
`;


export const Container = styled.div`
    width: auto;

    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-inline: 32px;
    padding-block: 16px;

    flex-wrap: wrap;
    gap: 8px;


    padding-top: 0px;

`;


export const TitleWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    gap: 16px;
    flex-wrap: wrap;
    align-items: center;
    button {
        height: 30px;
    }

`;

export const Title = styled.h1`
    font-weight: normal;
    text-align: start;
`;

export const Action = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    gap: 8px;
    flex-wrap: wrap;
    *{
        padding: 0px;
    }
`;
