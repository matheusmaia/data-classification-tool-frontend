import { AddOutlined } from "@mui/icons-material";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import AppBar from "../../components/AppBar";
import api from "../../services/api";
import { Main, DataGrid, Container, TitleWrapper, Title, Action } from "./styles";


const Home = () => {
    const navigate = useNavigate()

    useEffect(() => {
        if (localStorage.getItem('@User:id') === null) navigate(`/login`)
    }, []);

    const [rows, setRows] = useState([])

    const [open, setOpen] = useState(false);

    const [formValue, setformValue] = useState({
        name: '',
        description: '',
        label: '',
        user_id: localStorage.getItem('@User:id'),
        author: "Matheus Maia",
    });

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = async () => {
        const data = {
            name: formValue.name,
            description: formValue.description,
            label: formValue.name.replace(/\s/g, '').toLowerCase(),
            user_id: formValue.user_id,
            author: formValue.author
        }

        try {
            await api.post('/projects/', data);
            setOpen(false);

        } catch (error) {
            console.log(error);
        }
    }

    const handleChange = (event: any) => {
        setformValue({
            ...formValue,
            [event.target.name]: event.target.value
        });
    }


    const columns = [
        { field: 'id', headerName: 'ID' },
        {
            field: 'name',
            headerName: 'Name',
            flex: 1,
        },
        {
            field: 'description',
            headerName: 'Descrição',
            flex: 2,
        },
        {
            field: 'dataset_total',
            headerName: 'Tamanho (linhas)',
            flex: 1,
        },
        {
            field: 'variables_label',
            headerName: 'Variavel Alvo',
            flex: 1,
        },
        {
            field: 'author',
            headerName: 'Criado por',
            flex: 1,
        }
    ];

    useEffect(() => {

        api.get(`/users/` + localStorage.getItem('@User:id') + `/projects`).then(filters => {
            setRows(filters.data);
        })
    }, [open])

    const handleRowClick = (event: any) => {
        console.log(event.id);
        navigate(`/projetos/${event.id}`)
    }



    return (
        <>
            <AppBar />

            <Container>
                <TitleWrapper>
                    <Title> Meus Projetos </Title>
                </TitleWrapper>

                <Action>
                    <Button variant="contained" size="medium" startIcon={<AddOutlined />} onClick={handleClickOpen} disableElevation > Novo Projeto </Button>
                </Action>
            </Container>

            <DataGrid
                rows={rows}
                columns={columns}
                pageSize={7}
                rowsPerPageOptions={[10]}
                onRowClick={handleRowClick}
                disableSelectionOnClick
            />

            <Dialog open={open} onClose={handleClose} maxWidth={'xs'} >
                <DialogTitle>Criar Novo Projeto</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Formulario para cadastrar novo projeto.
                    </DialogContentText>


                    <TextField
                        style={{ marginTop: '20px' }}
                        label="Nome"
                        variant='outlined'
                        size="small"
                        fullWidth
                        name="name"
                        onChange={handleChange}

                    />

                    <TextField
                        style={{ marginTop: '20px' }}
                        label="Descrição"
                        variant='outlined'
                        fullWidth
                        size="small"
                        name="description"
                        onChange={handleChange}

                    />

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleSubmit}>Criar Projeto</Button>
                </DialogActions>
            </Dialog>
        </>
    )
}

export default Home;