import axios from 'axios'

// Pode ser algum servidor executando localmente:
// baseURL: 'http://127.0.0.1:5000'
//   


const api = axios.create({
  baseURL: 'https://dctbackend.herokuapp.com'
})

export default api
